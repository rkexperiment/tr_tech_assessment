﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public enum CurdOperation
    {
        Create,
        Read,
        Update,
        Delete
    }
}
