﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class CountryEntity : TableEntity
    {
        public CountryEntity(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
            this.Timestamp = DateTime.UtcNow;
        }
        public CountryEntity() { }
        public string Name { get; set; }
    }
}
