﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class MimeTypeEntity : TableEntity
    {
        public MimeTypeEntity(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
            this.Timestamp = DateTime.UtcNow;
        }
        public MimeTypeEntity() { }

        public string MimeType { get; set; }

    }
}
