﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class AssertEntity : TableEntity
    {
        public AssertEntity(string name, string assertId)
        {
            this.PartitionKey = name;
            this.RowKey = assertId;
            this.Timestamp = DateTime.UtcNow;
        }
        public AssertEntity() { }
        //public Guid Id { get; set; }
        public Guid MimeTypeId { get; set; }
        public Guid CountryId { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
    }
}
