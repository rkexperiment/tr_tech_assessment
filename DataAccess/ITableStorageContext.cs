﻿using DataAccess.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface ITableStorageContext
    {
        Task DeleteAssertTable();
        Task CreateAssertTable();
        Task TableBulkInsert<T>(List<T> entities) where T : ITableEntity;
        Task TableOperations<T>(T entity, CurdOperation operation) where T : ITableEntity;
        Task DeleteAssertRows(List<string> assertIds);
        Task<IEnumerable<AssertEntity>> FetchAsserts(int numOfPage = 50, int page = 1);
        Task<AssertEntity> FetchAssert(string rowKey);
        Task<IEnumerable<CountryEntity>> FetchCountries();
        Task<IEnumerable<MimeTypeEntity>> FetchMimeTypes();
        Task<int> GetTotalAsserts();

    }
}
