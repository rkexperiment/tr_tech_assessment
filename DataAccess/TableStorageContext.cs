﻿using DataAccess.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess
{
    public class TableStorageContext : ITableStorageContext
    {
        private readonly CloudStorageAccount storageAccount;
        private readonly CloudTableClient tableClient;
        private const string _assertEntity = "Assert";
        public TableStorageContext(string StorageConnectionString)
        {
            storageAccount = CloudStorageAccount.Parse(StorageConnectionString);
            tableClient = storageAccount.CreateCloudTableClient();
        }


        public async Task DeleteAssertTable()
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            await table.DeleteIfExistsAsync();
        }

        public async Task CreateAssertTable()
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            await table.CreateIfNotExistsAsync();
        }

        public async Task TableBulkInsert<T>(List<T> entities) where T : ITableEntity
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);

            var chunkEntities = entities.Select((x, i) => new { Index = i, Value = x })
             .GroupBy(x => x.Index / 30)
             .Select(x => x.Select(v => v.Value).ToList())
             .ToList();

            foreach (var chunkEnts in chunkEntities)
            {
                TableBatchOperation batchOperation = new TableBatchOperation();
                try
                {
                    foreach (var entity in chunkEnts)
                    {
                        batchOperation.Insert(entity);
                    }
                    await table.ExecuteBatchAsync(batchOperation);
                }
                catch (Exception ex)
                {

                }
            }

        }

        public async Task<AssertEntity> FetchAssert(string rowKey)
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            TableOperation retrieveOperation = TableOperation.Retrieve<AssertEntity>(_assertEntity, rowKey);
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);
            return (AssertEntity)retrievedResult.Result;
        }

        public async Task TableOperations<T>(T entity, CurdOperation operation) where T : ITableEntity
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            TableOperation tableOperation = null;
            switch (operation)
            {
                case CurdOperation.Create:
                    tableOperation = TableOperation.Insert(entity);
                    break;
                case CurdOperation.Update:
                    tableOperation = TableOperation.Replace(entity);
                    break;
                case CurdOperation.Delete:
                    tableOperation = TableOperation.Delete(entity);
                    break;
            }
            await table.ExecuteAsync(tableOperation);
        }

        public async Task<IEnumerable<AssertEntity>> FetchAsserts(int numOfPage = 50, int page = 1)
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            await table.CreateIfNotExistsAsync();

            TableQuery<AssertEntity> tableQuery = new TableQuery<AssertEntity>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _assertEntity));

            TableQuerySegment<AssertEntity> tableQueryResult = null;
            if (page == 1)
            {
                tableQuery.Take(numOfPage);
                tableQueryResult =
                    await table.ExecuteQuerySegmentedAsync(tableQuery, null);
                return tableQueryResult.Results;
            }
            tableQuery.Take(numOfPage * page - 1);
            tableQuery.SelectColumns = new List<string>() { "PartitionKey" };
            tableQueryResult =
                     await table.ExecuteQuerySegmentedAsync(tableQuery, null);
            TableContinuationToken continuationTokenNext = tableQueryResult.ContinuationToken;

            tableQuery = new TableQuery<AssertEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _assertEntity));
            tableQuery.Take(numOfPage);
            tableQueryResult = await table.ExecuteQuerySegmentedAsync(tableQuery, continuationTokenNext);

            return tableQueryResult.Results.OrderByDescending(a => a.Timestamp);
        }

        public async Task DeleteAssertRows(List<string> assertIds)
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            foreach (var rowKey in assertIds)
            {
                var entity = new DynamicTableEntity(_assertEntity, rowKey);
                entity.ETag = "*";
                await table.ExecuteAsync(TableOperation.Delete(entity));
            }
        }

        public async Task<int> GetTotalAsserts()
        {
            int totalCount = 0;
            //CloudTable table = tableClient.GetTableReference(_assertEntity);
            //TableQuery<AssertEntity> tableQuery = new TableQuery<AssertEntity>()
            //    .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _assertEntity));
            //tableQuery.SelectColumns = new List<string>() { "PartitionKey" };

            //TableContinuationToken continuationTokenNext = null;
            //do
            //{
            //    var tableQueryResult =
            //            await table.ExecuteQuerySegmentedAsync(tableQuery, continuationTokenNext);
            //    continuationTokenNext = tableQueryResult.ContinuationToken;
            //    totalCount += tableQueryResult.Results.Count;
            //} while (continuationTokenNext != null);

            return totalCount;
        }

        public async Task<IEnumerable<CountryEntity>> FetchCountries()
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            TableQuery<CountryEntity> query = new TableQuery<CountryEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "Country"));
            var taskResults = await table.ExecuteQuerySegmentedAsync(query, null);
            return taskResults.Results;
        }
        public async Task<IEnumerable<MimeTypeEntity>> FetchMimeTypes()
        {
            CloudTable table = tableClient.GetTableReference(_assertEntity);
            TableQuery<MimeTypeEntity> query = new TableQuery<MimeTypeEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "MimeType"));
            var taskResults = await table.ExecuteQuerySegmentedAsync(query, null);
            return taskResults.Results;
        }

    }
}
