﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebExperience.BusinessService.Models
{
    public class Assert
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset UpdatedOn { get; set; }
        public Country Country { get; set; }
        public MimeType MimeType { get; set; }




    }
}
