﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebExperience.BusinessService.Models
{
    public class MimeType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
