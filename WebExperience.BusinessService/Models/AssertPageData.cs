﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebExperience.BusinessService.Models
{
    public class AssertPageData
    {
        public int TotalAsserts { get; set; }
        public List<Assert> Asserts { get; set; }
        public List<Country> Countries { get; set; }
        public List<MimeType> MimeTypes { get; set; }
    }
}
