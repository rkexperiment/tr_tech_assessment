﻿using DataAccess;
using DataAccess.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExperience.BusinessService.Models;

namespace WebExperience.BusinessService
{
    public class AssertBusinessService : IAssertBusinessService
    {
        private readonly ITableStorageContext _iTableStorageContext;
        public AssertBusinessService(ITableStorageContext iTableStorageContext)
        {
            this._iTableStorageContext = iTableStorageContext;
        }

        public async Task<AssertPageData> GetAssertPageData()
        {
            var assertPageData = new AssertPageData();
           // assertPageData.TotalAsserts = await _iTableStorageContext.GetTotalAsserts();
            var asserts = await _iTableStorageContext.FetchAsserts();
            var countries = await _iTableStorageContext.FetchCountries();
            var mimeTypes = await _iTableStorageContext.FetchMimeTypes();
            assertPageData.Asserts = asserts.Select(a => new Assert()
            {
                Id = Guid.Parse(a.RowKey),
                Email = a.Email,
                CreatedBy = a.CreatedBy,
                Description = a.Description,
                Name = a.Name,
                UpdatedOn = a.Timestamp,
                Country = countries.Where(c => c.RowKey == a.CountryId.ToString()).Select(c => new Country() { Id = Guid.Parse(c.RowKey), Name = c.Name }).FirstOrDefault(),
                MimeType = mimeTypes.Where(c => c.RowKey == a.MimeTypeId.ToString()).Select(c => new MimeType() { Id = Guid.Parse(c.RowKey), Name = c.MimeType }).FirstOrDefault()
            }).ToList();

            assertPageData.Countries = countries.Select(a => new Country() { Id = Guid.Parse(a.RowKey), Name = a.Name }).ToList();
            assertPageData.MimeTypes = mimeTypes.Select(a => new MimeType() { Id = Guid.Parse(a.RowKey), Name = a.MimeType }).ToList();

            return assertPageData;
        }

        public async Task<IEnumerable<Assert>> FetchAsserts(int numOfPage = 50, int page = 1)
        {
            var asserts = await _iTableStorageContext.FetchAsserts(numOfPage, page);


            var countries = await _iTableStorageContext.FetchCountries();
            var mimeTypes = await _iTableStorageContext.FetchMimeTypes();
            var assertData = asserts.Select(a => new Assert()
            {
                Id = Guid.Parse(a.RowKey),
                Email = a.Email,
                CreatedBy = a.CreatedBy,
                Description = a.Description,
                Name = a.Name,
                UpdatedOn = a.Timestamp,
                Country = countries.Where(c => c.RowKey == a.CountryId.ToString()).Select(c => new Country() { Id = Guid.Parse(c.RowKey), Name = c.Name }).FirstOrDefault(),
                MimeType = mimeTypes.Where(c => c.RowKey == a.MimeTypeId.ToString()).Select(c => new MimeType() { Id = Guid.Parse(c.RowKey), Name = c.MimeType }).FirstOrDefault()
            });
            return assertData;
        }

        public async Task<IEnumerable<Assert>> ManageAssert(Assert assert, CurdOperation operation, int recordsPerPage = 50, int page = 1)
        {
            switch (operation)
            {
                case CurdOperation.Create:
                    return await CreateAssert(assert, recordsPerPage, page);
                case CurdOperation.Update:
                    return await UpdateAssert(assert, recordsPerPage, page);
                default:
                    return null;
            }
        }

        public async Task<IEnumerable<Assert>> DeleteAsserts(List<string> assertIds, int recordsPerPage = 50, int page = 1)
        {
            await _iTableStorageContext.DeleteAssertRows(assertIds);
            return await FetchAsserts(recordsPerPage, page);
        }

        private async Task<IEnumerable<Assert>> CreateAssert(Assert assert, int recordsPerPage = 50, int page = 1)
        {
            var assertEntity = new AssertEntity("Assert", Guid.NewGuid().ToString())
            { CountryId = assert.Country.Id, CreatedBy = "Owner", Description = assert.Description, Email = assert.Email, MimeTypeId = assert.MimeType.Id, Name = assert.Name };

            await _iTableStorageContext.TableOperations(assertEntity, CurdOperation.Create);

            return await FetchAsserts(recordsPerPage, page);

        }

        private async Task<IEnumerable<Assert>> UpdateAssert(Assert assert, int recordsPerPage = 50, int page = 1)
        {
            var assertEntity = await _iTableStorageContext.FetchAssert(assert.Id.ToString());
            assertEntity.Name = assert.Name;
            assertEntity.MimeTypeId = assert.MimeType.Id;
            assertEntity.CountryId = assert.Country.Id;
            assertEntity.Email = assert.Email;
            assertEntity.Description = assert.Description;
            await _iTableStorageContext.TableOperations(assertEntity, CurdOperation.Update);
            return await FetchAsserts(recordsPerPage, page);
        }


        public async Task<int> GetTotalAsserts()
        {
            return await _iTableStorageContext.GetTotalAsserts();
        }

    }
}
