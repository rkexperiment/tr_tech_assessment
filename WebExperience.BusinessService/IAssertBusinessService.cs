﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebExperience.BusinessService.Models;

namespace WebExperience.BusinessService
{
    public interface IAssertBusinessService
    {
        Task<AssertPageData> GetAssertPageData();
        Task<IEnumerable<Assert>> FetchAsserts(int numOfPage = 50, int page = 1);
        Task<int> GetTotalAsserts();
        Task<IEnumerable<Assert>> ManageAssert(Assert assert, CurdOperation operation, int recordsPerPage = 50, int page = 1);
        Task<IEnumerable<Assert>> DeleteAsserts(List<string> assertIds, int recordsPerPage = 50, int page = 1);
    }
}
