﻿using CsvHelper;
using DataAccess;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Threading.Tasks;


namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            /// var csvFile = Resources.AssetImport;

            Console.WriteLine($"Csv Process start time:{DateTime.UtcNow.ToShortTimeString()}");

            TableStorageContext tableStorageContext = new TableStorageContext(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            Console.WriteLine($"Deleting existing assert table");
            tableStorageContext.DeleteAssertTable().Wait();
            Thread.Sleep(60000);
            Console.WriteLine($"Creating assert table");
            tableStorageContext.CreateAssertTable().Wait();// To do check table is exists or not
            var textReader = new StringReader(Resources.AssetImport);
            var csv = new CsvReader(textReader);

            var records = csv.EnumerateRecords(new AssertSchema());

            List<AssertEntity> asserts = new List<AssertEntity>();
            List<CountryEntity> countries = new List<CountryEntity>();
            List<MimeTypeEntity> mimeTypes = new List<MimeTypeEntity>();

            foreach (var assert in records)
            {
                CountryEntity country = countries.Any(c => c.Name == assert.country)
                                        ? countries.FirstOrDefault(c => c.Name == assert.country)
                                        : new CountryEntity("Country", Guid.NewGuid().ToString()) { Name = assert.country };

                MimeTypeEntity mimeType = mimeTypes.Any(c => c.MimeType == assert.mime_type)
                                        ? mimeTypes.FirstOrDefault(c => c.MimeType == assert.mime_type)
                                        : new MimeTypeEntity("MimeType", Guid.NewGuid().ToString()) { MimeType = assert.mime_type };

                if (!mimeTypes.Any(c => c.MimeType == assert.mime_type))
                    mimeTypes.Add(mimeType);

                if (!countries.Any(c => c.Name == assert.country))
                    countries.Add(country);

                asserts.Add(new AssertEntity("Assert", assert.asset_id)
                {
                    Name = assert.file_name,
                    CountryId = Guid.Parse(country.RowKey),
                    CreatedBy = assert.created_by,
                    Description = assert.description,
                    Email = assert.email,
                    MimeTypeId = Guid.Parse(mimeType.RowKey)
                });
            }

            Console.WriteLine($"Inserting Asserts,Contries and MimeTypes");
            tableStorageContext.TableBulkInsert(asserts).Wait();
            tableStorageContext.TableBulkInsert(countries).Wait();
            tableStorageContext.TableBulkInsert(mimeTypes).Wait();

            Console.WriteLine($"Asserts:{asserts.Count}");
            Console.WriteLine($"Contries:{countries.Count}");
            Console.WriteLine($"MimeTypes:{mimeTypes.Count}");

            Console.WriteLine($"Csv Process endtime:{DateTime.UtcNow.ToShortTimeString()}");
            textReader.Close();

        }
    }


    public class AssertSchema
    {
        public string asset_id { get; set; }
        public string file_name { get; set; }
        public string mime_type { get; set; }
        public string created_by { get; set; }
        public string email { get; set; }
        public string country { get; set; }
        public string description { get; set; }
    }
}
