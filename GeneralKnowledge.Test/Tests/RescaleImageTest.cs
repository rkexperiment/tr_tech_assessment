﻿using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)
            Console.WriteLine("***Image Rescale***");
            int imgHeight = 1200;
            int imgWidth = 1600;
            string imgUrl = "http://imageprocessor.org/assets/img/originals/leaf.jpg";
            Byte[] byteArray = null;
            string extension = string.Empty;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(imgUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;
                WebResponse webResponse = webRequest.GetResponse();
                var stream = webResponse.GetResponseStream();
                byteArray = ReadFully(stream);
                using (MemoryStream inStream = new MemoryStream(byteArray))
                {
                    var image = Image.FromStream(inStream);
                    extension = new ImageFormatConverter().ConvertToString(image.RawFormat);
                    image.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"originalImage.{extension}"));
                }

                webResponse.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to download the image from :{imgUrl}");
            }

            if (byteArray != null)
            {
                using (MemoryStream inStream = new MemoryStream(byteArray))
                using (MemoryStream outStream = new MemoryStream())
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {
                        var r = new ResizeLayer(new Size(imgWidth, imgHeight), ResizeMode.Stretch);
                        imageFactory.Load(inStream)
                           .Resize(r)
                           .Save(outStream);
                    }
                    var image = Image.FromStream(outStream);
                    image.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"rescaledImage.{extension}"));
                }
            }

            Console.WriteLine($"Find the Original image download path:{Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"originalImage.{extension}")}");
            Console.WriteLine($"Find the Rescaled image download path:{Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"rescaledImage.{extension}")}");
        }

        private byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
