﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z
            Console.WriteLine("***XML Reader***");
            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            Console.WriteLine("Parameter     LOW    AVG   MAX");
            string[] parameters = new string[] { "temperature", "pH", "Chloride", "Phosphate", "Nitrate" };
            foreach (var param in parameters)
                PrintParameters(doc, param);
        }

        private void PrintParameters(XmlDocument doc, string paramName)
        {
            var nodes = doc.SelectNodes($"samples/measurement/param[@name='{paramName}']");
            List<double> values = new List<double>();
            foreach (XmlNode node in nodes)
                values.Add(Convert.ToDouble(node.InnerText));

            values = values.OrderBy(d => d).ToList();
            Console.WriteLine($"{paramName}  {values.FirstOrDefault()}  {Math.Round(values.Average(), 2)} {values.LastOrDefault()}");
        }


    }
}
