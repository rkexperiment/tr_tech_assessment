﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO:
            // Complete the methods below
            Console.WriteLine("***String Test***");
            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stopp";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";

            // TODO:
            // Write an algoritm that gets the unique characters of the word below 
            // and counts the number of occurences for each character found
            foreach (var group in word.GroupBy(c => c))
            {
                Console.WriteLine($"{group.Key} : {group.Count()}");
            }

        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO: 
            // Write logic to determine whether a is an anagram of b
            if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
                return false;

            string source = String.Concat(a.ToLower().OrderBy(c => c)).FilterAlphanumerics();
            string anagram = String.Concat(b.ToLower().OrderBy(c => c)).FilterAlphanumerics();

            if (source.Length == anagram.Length)
                return source == anagram;

            if (anagram.Length < source.Length)
            {
                var anagramCharsGroups = anagram.GroupBy(c => c);
                var sourceGroups = source.GroupBy(c => c);
                foreach (var aGroup in anagramCharsGroups)
                {
                    if (!sourceGroups.Any(aC => aC.Key == aGroup.Key) || aGroup.Count() > sourceGroups.FirstOrDefault(aC => aC.Key == aGroup.Key).Count())
                        return false;
                }
                return true;
            }

            return false;
        }

        public static string FilterAlphanumerics(this string source)
        {
            return new String(source.Trim().Where(c => Char.IsLetter(c) || Char.IsNumber(c)).ToArray());
        }

    }
}
