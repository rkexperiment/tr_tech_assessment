﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebExperience.BusinessService;
using WebExperience.Test.Response;
using WebExperience.Test.Request;

namespace WebExperience.Test.Controllers
{
    [Produces("application/json")]
    [Route("api/Assert")]
    public class AssertController : Controller
    {
        private readonly IAssertBusinessService _iAssertBusinessService;
        public AssertController(IAssertBusinessService iAssertBusinessService)
        {
            _iAssertBusinessService = iAssertBusinessService;
        }


        [HttpGet("GetAssertPage")]
        public async Task<AssertPageResponse> GetAssertPage()
        {
            var response = new AssertPageResponse();
            try
            {
                response.IsSuccess = true;
                response.AssertPageData = await _iAssertBusinessService.GetAssertPageData();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Exception = ex.ToString();
            }
            return response;
        }

        [HttpPost("ManageAsserts")]
        public async Task<AssertDataResponse> ManageAsserts([FromBody]ManageAssertRequest request)
        {
            var response = new AssertDataResponse();
            try
            {
                response.IsSuccess = true;
                response.Asserts = await _iAssertBusinessService.ManageAssert(request.Assert, request.Operation, request.RecordsPerPage, request.Page);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Exception = ex.ToString();
            }
            return response;
        }

        [HttpPost("DeleteAsserts")]
        public async Task<AssertDataResponse> DeleteAsserts([FromBody]DeleteAssertRequest request)
        {
            var response = new AssertDataResponse();
            try
            {
                response.IsSuccess = true;
                response.Asserts = await _iAssertBusinessService.DeleteAsserts(request.AssertIds, request.RecordsPerPage, request.Page);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Exception = ex.ToString();
            }
            return response;
        }

        [HttpPost("GetAsserts")]
        public async Task<AssertDataResponse> GetAsserts([FromBody] GetAssertRequest request)
        {
            var response = new AssertDataResponse();
            try
            {
                response.IsSuccess = true;
                response.Asserts = await _iAssertBusinessService.FetchAsserts(request.RecordsPerPage, request.Page);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Exception = ex.ToString();
            }
            return response;
        }

    }
}