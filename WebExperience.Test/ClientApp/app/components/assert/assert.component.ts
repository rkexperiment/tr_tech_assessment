﻿import { Component, Inject, OnInit } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { FormGroup, FormControl, FormArray, NgForm, Validators } from '@angular/forms';

@Component({
    selector: 'assert',
    templateUrl: './assert.component.html'
})
export class AssertComponent implements OnInit {

    public asserts: any[];
    public countries: any[];
    public mimeTypes: any[];
    public isDetailView: boolean = false;
    public isEditMode: boolean = false;
    private assertForm: FormGroup;
    private editingAssert: any;
    public isUpdating: boolean = false;
    public showSuccessMsg: boolean = false;
    public totalAsserts: number = 0;
    public isDeleting: boolean = false;
    public recordsPerPage: number = 50;
    public page: number = 1;
    public isFetchingAsserts: boolean = false;
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {
        this.isFetchingAsserts = true;
        this.http.get(baseUrl + 'api/Assert/GetAssertPage').subscribe(result => {
            this.isFetchingAsserts = false;
            var obj = result.json();
            if (obj.isSuccess) {
                this.asserts = obj.assertPageData.asserts;
                this.totalAsserts = obj.assertPageData.totalAsserts;
                this.asserts.forEach(a => a.checked = false);
                this.countries = obj.assertPageData.countries;
                this.mimeTypes = obj.assertPageData.mimeTypes;

            }

        }, error => console.error(error));
    }

    ngOnInit(): void {
        this.assertForm = new FormGroup({
            'name': new FormControl('', Validators.required),
            'email': new FormControl('', Validators.required),
            'description': new FormControl(),
            'country': new FormControl(),
            'mimeType': new FormControl()
        });

    }

    openDetailView(assert: any) {
        this.showSuccessMsg = false;
        this.isDetailView = true;
        if (assert == null) {
            this.editingAssert = null;
            this.isEditMode = false;
            this.assertForm.patchValue({ name: '', email: '', description: '', country: this.countries[0].id, mimeType: this.mimeTypes[0].id });
        }
        else {
            this.editingAssert = assert;
            this.isEditMode = true;
            this.assertForm.patchValue({ name: assert.name, email: assert.email, description: assert.description, country: assert.country.id, mimeType: assert.mimeType == null ? this.mimeTypes[0].id : assert.mimeType.id });
        }
    };

    enableDelete() {
        if (this.asserts == null)
            return false;
        return this.asserts.find(a => a.checked);
    }

    onDelete() {
        var checkedAsserts = this.asserts.find(a => a.checked);
    }

    onSubmit(assertForm: NgForm) {
        this.showSuccessMsg = false;
        this.isUpdating = true;
        var country = this.countries.find(c => c.id == assertForm.value.country);
        var mimeType = this.mimeTypes.find(c => c.id == assertForm.value.mimeType);
        if (this.editingAssert) {
            this.editingAssert.name = assertForm.value.name;
            this.editingAssert.email = assertForm.value.email;
            this.editingAssert.description = assertForm.value.description;
            this.editingAssert.country = country
            this.editingAssert.mimeType = mimeType
        } else {
            this.editingAssert = {
                name: assertForm.value.name,
                email: assertForm.value.email,
                description: assertForm.value.description,
                country: country,
                mimeType: mimeType
            };
        }

        let postData = {
            Assert: this.editingAssert,
            Operation: this.isEditMode ? 2 : 0,
            RecordsPerPage: 50,
            Page: 1
        }
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        this.http.post(this.baseUrl + 'api/Assert/ManageAsserts', JSON.stringify(postData), options).subscribe(result => {
            this.isUpdating = false;
            this.showSuccessMsg = true;
            var obj = result.json();
            if (obj.isSuccess) {
                this.asserts = obj.asserts;
                this.totalAsserts = obj.totalAsserts;
                this.asserts.forEach(a => a.checked = false);
            }
        }, error => console.error(error));

    }

    onDeleteAsserts() {

        this.isDeleting = true;
        let postAssertIds: any[] = [];
        this.asserts.filter(a => a.checked).forEach((a) => postAssertIds.push(a.id));

        let postData = {
            AssertIds: postAssertIds,
            RecordsPerPage: 50,
            Page: 1
        }

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        this.http.post(this.baseUrl + 'api/Assert/DeleteAsserts', JSON.stringify(postData), options).subscribe(result => {
            this.isDeleting = false;
            var obj = result.json();
            if (obj.isSuccess) {
                this.asserts = obj.asserts;
                this.totalAsserts = obj.totalAsserts;
                this.asserts.forEach(a => a.checked = false);
            }
        }, error => console.error(error));

    }
    onPageNumberChange(type: any) {
        if (type == 0 && this.page > 1)
            this.page -= 1;
        else
            this.page += 1;
        this.fetchAsserts();
    }

    fetchAsserts() {
        this.isFetchingAsserts = true;
        this.asserts = [];
        let postData = {
            RecordsPerPage: this.recordsPerPage,
            Page: this.page
        }
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        this.http.post(this.baseUrl + 'api/Assert/GetAsserts', JSON.stringify(postData), options).subscribe(result => {
            this.isFetchingAsserts = false;
            var obj = result.json();
            if (obj.isSuccess) {
                this.asserts = obj.asserts;
                this.asserts.forEach(a => a.checked = false);
            }
        }, error => console.error(error));
    }
}