﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExperience.BusinessService.Models;

namespace WebExperience.Test.Response
{
    public class AssertDataResponse
    {
        public bool IsSuccess { get; set; }
        public IEnumerable<Assert> Asserts { get; set; }
        public int TotalAsserts { get; set; }
        public string Exception { get; set; }
    }
}
