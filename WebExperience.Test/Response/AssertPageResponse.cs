﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExperience.BusinessService.Models;

namespace WebExperience.Test.Response
{
    public class AssertPageResponse
    {
        public bool IsSuccess { get; set; }
        public AssertPageData AssertPageData { get; set; }
        public string Exception { get; set; }
    }
}
