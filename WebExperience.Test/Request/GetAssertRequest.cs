﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebExperience.Test.Request
{
    public class GetAssertRequest
    {
        public int RecordsPerPage { get; set; }
        public int Page { get; set; }
    }
}
