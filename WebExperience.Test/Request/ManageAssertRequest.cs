﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExperience.BusinessService.Models;

namespace WebExperience.Test.Request
{
    public class ManageAssertRequest
    {
        public Assert Assert { get; set; }
        public CurdOperation Operation { get; set; }
        public int RecordsPerPage { get; set; }
        public int Page { get; set; }
    }
}
